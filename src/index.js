import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
// import AppNavbar from "./components/AppNavbar";

// Import the Bootstrap CSS
import "bootstrap/dist/css/bootstrap.min.css";

const root = ReactDOM.createRoot(document.getElementById("root"));

root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals

// const name = "Levin John Alvarez";

// const student = {
//   firstName: "Rico",
//   lastName: "Tasong",
// };

// function userName(user) {
//   return user.firstName + ` ` + user.lastName;
// }

// const element = <h1>Hello, {userName(student)}</h1>;

// root.render(element);
